# DWeb Camp 2022

![campfire](images/storytelling.jpeg)

[DWeb Camp](https://dwebcamp.dev.archive.org/) is a collaborative space for people to connect, learn, share, and have fun as we work towards building a better, decentralized web. A web that is more open, private, and secure. A web with many winners, home to many voices

This is the repository for the organizing of DWeb Camp from August 24-28, 2022.

## Get there
![august-24-28](https://img.shields.io/badge/august%2024-28-ffcd03.svg)

See the [DWeb Camp website](https://dwebcamp.org) for event and registration details. This event has a [Code of Conduct](https://dwebcamp.org/conduct/).

After you have registered, these are some handy pages for coordinating logistics:

- Want to share your tent? Here's an informal [Cabin and Tent Sharing sheet](https://docs.google.com/spreadsheets/d/1xnXoOIzQdvS-jQwQnNCdd47ELj07LAaFZQYb9Bkwk_Y) to help you find roommates.

- [Rideshare page](https://docs.google.com/spreadsheets/d/1p4wPxLb3tBChn72zQh_EbI5D_6xxEM678Cl59KUnzfo/edit?usp=sharing) for people looking to offer/need rides to and from Camp. If you have 3 people in your car, parking is free. Look for the tabs at the bottom for each day.


## Get Involved

![help-wanted](https://img.shields.io/badge/help%20wanted-ffcd03.svg)

We want to invite you to build DWeb Camp together. Please comment if you'd like to help!

| Roles                                    | Description                                                                    |
|:-----------------------------------------|:-------------------------------------------------------------------------------|
| Space Stewards | Runs each main space Thur-Fri-Sat           |
| Head Weavers              | Organizes the small group leaders |
| Weavers              | Volunteers who lead small groups (4-6 people each) in daily small group sessions |
| Music & Entertainment Coordinator | Books talent; coordinates AV; makes sure all the entertainment & music are where they need to be |
| Librarian | Creates camp library & serves as head librarian; recruits other reference librarians to help people find materials           |
| Kids Program Lead | Organizes outdoor activities for kids (archery, hiking, wall climbing, art)           |
| Creative Coordinator | Supports artists & creative programs           |
| Transportation organizer | Helps coordinate ride share & tent shares           |
| Videography team | To capture and upload videos of talks           |
| Photographer | To capture and document the event, top to toe           |
| Chalk Board artist/communicator | To help us create awesome chalk board signs & art           |
| Tea serving -- cool vibe & conversation | To help with hospitality in the evenings in the Redwood Cathedral Tea Tent           |
| Native & land historian | To help us understand and explore the Pomo roots of the land of Navarro Camp           |
| Storyteller | To tell stories and lead storytelling -for children and everyone         |
| Star Gazing Leader | The camp has an amazing stargazing hill about a 20 minute hike away; would love someone who can tell us which stars we are seeing and the stories behind them           |
| Nature Walk leaders | Do you know the flora, fauna and wildlife of Mendocino/Anderson Valley?           |
| Yoga, meditation, bodywork instructors |            |
| Sing-Along Leader |            |


Visit the [website](https://dwebcamp.org/participate/) to know more about ways to participate.

## Ask us anything
![q&a](https://img.shields.io/badge/questions-answers-ffcd03.svg)

We are holding monthly Q & A Sessions

| Session       | Time (in PDT) | Location | Notes |
|:--------------------|:-----------------------------|:---------------------------------------------------------------|:--------------------------------------------------------------|
| #1: About [DWeb Fellows](https://dwebcamp.org/fellowships/) Program | `Wed Apr 27, 2022 9:00 am – 10:00 am` | [📹 recording](https://archive.org/details/gmt-20220427-161109-recording-gvo-1280x-720) | [:memo: notes](https://gitlab.com/getdweb/dweb-camp-2022/-/blob/main/originizing/ask-us-anything-notes/aua-20220427.md) |
| #2: About DWeb Camp | `Tues May 10, 2022 8:00 am – 9:00 am` | [Zoom meeting](https://decentalizedweb.us20.list-manage.com/track/click?u=4b9b07f44a3c9768397a9cb9f&id=90ec07453f&e=5a9af6f581) |  |

Questions? Write to dwebcamp@archive.org 

Stay in touch via our [Matrix Channel](https://matrix.to/#/!WBhcGXTDMlzyTPWoJv:matrix.org?via=matrix.org&via=tomesh.net&via=privacytools.io).

## Timeline
|  Date    |    Event                                                           |
|:----------|:--------------------------------------------------------------|
| Aug 22-24 | Volunteer build                                      |
| Aug 24-28 | **DWeb Camp**                                                 |
| Aug 28-29 | Volunteer take down                 |

## Schedule

| Mon-Tues 8/22-24 | Wed 8/24 | Thu 8/25 | Fri 8/26 | Sat 8/27 | Sun 8/28 | Mon 8/29 |
| :--------- | :--------- | :--------- | :--------- | :--------- | :--------- | :--------- | 
| **BUILD (36-70 people)** | **CAMP DAY ONE** | **CAMP DAY TWO** | **CAMP DAY THREE** | **CAMP DAY FOUR** | **CAMP DAY FIVE** | **TAKE DOWN HALF DAY** | 
| 10:00 AM Core Team Arrives (Ben, Brady, etc.) | 8:00-9:00 AM Breakfast (Staff) | 8:00-9:30 AM Breakfast | 8:00-9:30 AM Breakfast | 8:00-9:30 AM Breakfast | 7:30-9:30 AM Self-Serve Breakfast | 9:00 AM Final Take Down |
| 1:00 PM Volunteers arrive | | 9:30-12:30 PM Morning Session | 9:30-12:30 PM Morning Session | 9:30-12:30 PM Morning Session | |
| | Noon-1:00 PM Lunch (Staff) | 12:30-2:00 PM Lunch | 12:30-2:00 PM Lunch | 12:30-2:00 PM Lunch | 10:00-11:30 AM Brunch | 
| | | | | | Noon-1:00 PM Closing Ceremony | 11:00 AM Must be done by this time |
| 3:00 PM Volunteer Orientation | | | | | 1:00-2:00 PM Pack up! Break down begins | |
| 4:00 PM Build Starts | | 2:00-4:30 PM Afternoon Session | 2:00-4:30 PM Afternoon Session | 2:00-4:30 PM Afternoon Session | 2:00 PM Camp Ends. *Campers must leave by this time!* | |
| | Gates Open. 2:00 PM Registration Opens | | | | *Take Down continues* | |
| | Set up tents; get settled | 4:30-6:30 PM Unscheduled Time | 4:30-6:30 PM Unscheduled Time | 4:30-6:30 PM Unscheduled Time | | |
| | | 5:30-6:30 PM Cocktail Hour | 5:30-6:30 PM Cocktail Hour | 5:30-6:30 PM Cocktail Hour | | |
| 6:30-8:30 PM Dinner | 6:30-8:30 PM Dinner | 6:30-8:30 PM Dinner | 6:30-8:30 PM Dinner | 6:30-8:30 PM Dinner | 6:00-8:00  PM Staff/Links Pizza Party | (*Leave no trace!*) |
| | 8:00 PM Opening Circle & Orientation (Everyone is encouraged to attend this) | 8:00-8:30 PM Small Groups meet | 8:00-8:30 PM Small Groups meet | 8:00-8:30 PM Small Groups meet | | |
| | 9:00-11:00 PM Evening activities | 8:30-11:00 PM Evening activities | 8:30-Midnight Evening Activities | 8:30-1:00 AM Evening Activities | | | 
| | Campfire ends each night at 2:00 AM | | | 10:30-Midnight Pizza snack | | |
| | 10:00 PM Registration Desk Closes | 8:00 PM Registration Desk Closes | 8:00 PM Registration Desk Closes | | | | 