### DWeb Camp 2022 forms and spreadsheets

Forms

- [Can you help us plan?](https://docs.google.com/forms/d/e/1FAIpQLSc-tHvsC2fbz929o_kklbqh2D1t_M-fusBYkSyIFSq2S5fohA/viewform?usp=sf_link)
- [Application to become a Space Steward](https://docs.google.com/forms/d/e/1FAIpQLSeFlum8ghGc4NF5FHxZK410qU78HrueISkfaPABPeKMi31lKA/viewform)
- [Application to become a Volunteer "Link"](https://docs.google.com/forms/d/e/1FAIpQLScB6wvXiaWz3V00_hQ1y1Lp7sFPByOciCflp-0_zkyuukd11Q/viewform)
- [DWeb Fellows Application](https://docs.google.com/forms/d/e/1FAIpQLSeL66tnskq40OWOCjfB24MuHT-KaUdrcDvKnuD0CZqmZi_znQ/viewform)
- [Propose a Project](https://docs.google.com/forms/d/e/1FAIpQLSfzhhPhcoiVQ_-Iltmwx34XihhfuugCeaNb-ILMWcTLw0wahg/viewform)
- [Propose a Talk](https://docs.google.com/forms/d/e/1FAIpQLSdoSmra1vmsIGkrpIVaS2JxRfpaO-vS-1NkFUNeQfTSMao-pw/viewform)

-----

Logistics spreadsheets

- [Cabin and Tent Sharing](https://docs.google.com/spreadsheets/d/1xnXoOIzQdvS-jQwQnNCdd47ELj07LAaFZQYb9Bkwk_Y)
- [Ride-sharing Tracker](https://docs.google.com/spreadsheets/d/1p4wPxLb3tBChn72zQh_EbI5D_6xxEM678Cl59KUnzfo/edit?usp=sharing)
